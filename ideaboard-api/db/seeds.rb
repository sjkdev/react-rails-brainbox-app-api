ideas = Idea.create(
  [
    {
      title: "A list of rails/ react based api web apps",
      body: "refer to asana list of ideas"
    },
    {
      title: "A twitter client idea",
      body: "Only for replying to mentions and DMs"
    },
    {
      title: "A MTG web app",
      body: "An app that plays games of xmage plus a deckbuilder section"
    },
    {
      title: "Passive income ideas",
      body: "Refer to list/ ideas on asana board"
    }
  ])